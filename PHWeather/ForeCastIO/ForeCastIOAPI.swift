//
//  ForeCastIOAPI.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 15/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation

struct ForeCastIOAPI {
    fileprivate static let APIURL = "https://api.forecast.io/forecast/"
    fileprivate static let APIKey = "8bf7699dcd5471adc2b75344268f8f22"
    
    static func currentWeatherURL(_ lat: Float, lon: Float, queryParams: [String: String]?) -> URL {
        let baseParams = ["exclude": "minutely,hourly,daily,alerts,flags"]
        
        var fullParams = [String: String]()
        
        for (key, value) in baseParams {
            fullParams[key] = value
        }
        
        if let params = queryParams, params.count > 0 {
            for (key, value) in params {
                fullParams[key] = value
            }
        }
        
        return foreCastIOURL(lat, lon: lon, parameters: fullParams)
    }
 
    fileprivate static func foreCastIOURL(_ lat: Float, lon: Float, parameters: [String: String]) -> URL {
        let baseURLString = APIURL + APIKey + "/\(lat),\(lon)"
        var components = URLComponents(string: baseURLString)!
        
        var queryItems = [URLQueryItem]()

        for (key, value) in parameters {
            let item = URLQueryItem(name: key, value: value)
            queryItems.append(item)
        }
        
        components.queryItems = queryItems
        
        print("URL formed: \(components.url?.absoluteString)")
        return components.url!
    }
    
    static func getWeatherFromJSONData(_ data: Data) -> WeatherJSONResult {
        do {
            let jsonObject: Any = try JSONSerialization.jsonObject(with: data, options: [])
            
            print(jsonObject)
            
            guard let jsonDictionary = jsonObject as? NSDictionary,
                let currentData = jsonDictionary["currently"] as? NSDictionary,
                let description = currentData["summary"] as? String,
                let temperature = currentData["temperature"] as? Float,
                let humidity = currentData["humidity"] as? Float,
                let apparentTemp = currentData["apparentTemperature"] as? Float
                else {
                    print("JSON extraction error")
                    return .failure(WeatherJSONError.rawJSONParseError)
            }
            
            // icon is nil for now
            let currentWeather = Weather(city: "Manila", temperature: temperature, humidity: humidity, description: description, icon: nil)
            
            print("temp: \(currentWeather.temperature), humidity: \(currentWeather.humidity), description: \(currentWeather.description)")
            
            return .success(currentWeather)
        } catch let error {
            return .failure(error)
        }
    }
}
