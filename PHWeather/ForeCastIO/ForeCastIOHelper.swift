//
//  ForecastIOHelper.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 15/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation

class ForeCastIOHelper {
    static let sharedInstance = ForeCastIOHelper()
    
    fileprivate let session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    func getCurrentWeather(_ completion: @escaping (WeatherJSONResult) -> Void) {
        let url = ForeCastIOAPI.currentWeatherURL(14.6, lon: 120.98, queryParams: nil)
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            let result = self.processCurrentWeatherRequest(data, error: error as NSError?)
            
            completion(result)
        }) 
        
        task.resume()
    }
    
    fileprivate func processCurrentWeatherRequest(_ data: Data?, error: NSError?) -> WeatherJSONResult {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return ForeCastIOAPI.getWeatherFromJSONData(jsonData)
    }
}
