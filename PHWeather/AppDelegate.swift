//
//  AppDelegate.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 14/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import UIKit
import Fabric
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Twitter.self])
        
        application.isStatusBarHidden = true
        
        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        let navController = window?.rootViewController as! UITabBarController
        let phWeatherVC = navController.viewControllers!.first as! PHWeatherViewController
        
        phWeatherVC.startLocServiceAndChangeBG()
    }
}

