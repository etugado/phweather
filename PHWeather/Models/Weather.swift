//
//  Weather.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 14/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation

struct Weather {
    let city: String
    let temperature: Float
    let humidity: Float
    let description: String
    let icon: String?
}