//
//  OpenWeatherHelper.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 14/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation
import CoreLocation

class OpenWeatherHelper {
    static let sharedInstance = OpenWeatherHelper()
    
    fileprivate let session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    func getCurrentWeatherForLocation(location: CLLocation, completion: @escaping WeatherCompletionHandler) {
        print(#function)
        
        let url = OpenWeatherMapAPI.weatherURLByCoordinates(location.coordinate)
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            let result = self.processCurrentWeatherRequest(data, error: error as NSError?)
            
            completion(result)
        }) 
        
        task.resume()
    }
    
    func getCurrentWeatherForManila(_ completion: @escaping WeatherCompletionHandler) {
        print(#function)
        
        let url = OpenWeatherMapAPI.weatherURLByCity("Manila")
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            let result = self.processCurrentWeatherRequest(data, error: error as NSError?)
            
            completion(result)
        }) 
        
        task.resume()
    }
    
    fileprivate func processCurrentWeatherRequest(_ data: Data?, error: NSError?) -> WeatherJSONResult {
        guard error == nil else {
            print("network error")
            return .failure(error!)
        }
        
        guard let jsonData = data else {
            print("raw json error")
            return .failure(WeatherJSONError.rawJSONParseError)
        }
        
        return OpenWeatherMapAPI.getWeatherFromJSONData(jsonData)
    }
    
    static let weatherIcons = [
        "01d" : "\u{f00d}",
        "01n" : "\u{f02e}",
        "02d" : "\u{f002}",
        "02n" : "\u{f086}",
        "03d" : "\u{f07d}",
        "03n" : "\u{f07e}",
        "04d" : "\u{f000}",
        "04n" : "\u{f022}",
        "09d" : "\u{f00b}",
        "09n" : "\u{f02b}",
        "10d" : "\u{f009}",
        "10n" : "\u{f026}",
        "11d" : "\u{f010}",
        "11n" : "\u{f02d}",
        "13d" : "\u{f065}",
        "13n" : "\u{f02a}",
        "50d" : "\u{f003}",
        "50n" : "\u{f04a}",
        "900" : "\u{f056}",
        "901" : "\u{f01d}",
        "902" : "\u{f056}"
    ]
}

enum DefaultWeatherIcons: String {
    case Day = "\u{f00d}"   // wi-day-sunny
    case Night = "\u{f02e}" // wi-night-clear
}
