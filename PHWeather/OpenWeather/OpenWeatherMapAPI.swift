//
//  OpenWeatherMapAPI.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 14/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation
import CoreLocation

struct OpenWeatherMapAPI {
    fileprivate static let APIURL = "http://api.openweathermap.org/data/2.5/weather"
    fileprivate static let APIKey = "25458e2bea8b119826fc149f84812e64"
    
    static func weatherURLByCity(_ name: String) -> URL {
        return OpenWeatherMapAPI.openWeatherURL(["q": name])
    }
    
    static func weatherURLByCoordinates(_ coordinates: CLLocationCoordinate2D) -> URL {
        return OpenWeatherMapAPI.openWeatherURL(["lat": String(coordinates.latitude), "lon": String(coordinates.longitude)])
    }
    
    static func getWeatherFromJSONData(_ data: Data) -> WeatherJSONResult {
        do {
            let jsonObject: Any = try JSONSerialization.jsonObject(with: data, options: [])
            
            print(jsonObject)
            
            guard let jsonDictionary = jsonObject as? NSDictionary,
                let city = jsonDictionary["name"] as? String,
                let main = jsonDictionary["main"] as? NSDictionary,
                let temperature = main["temp"] as? NSNumber,
                let humidity = main["humidity"] as? NSNumber,
                let weather = jsonDictionary["weather"] as? NSArray,
                let descGrp = weather.firstObject as? NSDictionary,
                let description = descGrp["description"] as? String,
                let iconCode = descGrp["icon"] as? String
            else {
                print("JSON extraction error")
                return .failure(WeatherJSONError.rawJSONParseError)
            }
        
            var weatherIcon = OpenWeatherHelper.weatherIcons[iconCode]

            if weatherIcon == nil {
                let partOfDay = WeatherHelper.currentPartOfDay()

                if partOfDay == PartOfDay.am {
                    weatherIcon = DefaultWeatherIcons.Day.rawValue
                } else {
                    weatherIcon = DefaultWeatherIcons.Night.rawValue
                }
            }
            
            let currentWeather = Weather(city: city, temperature: temperature.floatValue, humidity: humidity.floatValue, description: description, icon: weatherIcon!)
            
            print("city: \(city), temp: \(currentWeather.temperature), humidity: \(currentWeather.humidity), description: \(currentWeather.description)")
            
            return .success(currentWeather)
        } catch let error {
            return .failure(error)
        }
    }
    
    fileprivate static func openWeatherURL(_ parameters: [String:String]) -> URL {
        var components = URLComponents(string: APIURL)!
        
        var queryItems = [URLQueryItem]()
        
        let metricUnitsQueryItem = URLQueryItem(name: "units", value: "imperial")
        queryItems.append(metricUnitsQueryItem)
        let apiQueryItem = URLQueryItem(name: "appID", value: APIKey)
        queryItems.append(apiQueryItem)
        
        for (key, value) in parameters {
            let item = URLQueryItem(name: key, value: value)
            queryItems.append(item)
        }
        
        components.queryItems = queryItems
        
        return components.url!
    }
}
