//
//  LocationService.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 15/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation
import CoreLocation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LocationService: NSObject, CLLocationManagerDelegate {
    fileprivate let locationManager = CLLocationManager()
    
    var locationFoundHandler: WeatherCompletionHandler!
    
    override init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500
    }
    
    func startLocationServiceWithCompletionHandler(_ locationFoundHandler: @escaping WeatherCompletionHandler) {
        self.locationFoundHandler = locationFoundHandler
        
        let appAuthorization = CLLocationManager.authorizationStatus()
        trackLocationIfProperAuthorization(currentAuthorization: appAuthorization)
    }
    
    fileprivate func trackLocationIfProperAuthorization(currentAuthorization: CLAuthorizationStatus) {
        switch currentAuthorization {
        case .restricted, .denied:
            print("location services restricted for app")
        case .notDetermined:
            print("not determined")
            locationManager.requestWhenInUseAuthorization()
        case .authorizedAlways, .authorizedWhenInUse:
            print("authorized")
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(#function)
        
        guard let currentLocation = locations.last, let locFoundHander = locationFoundHandler else {
            return
        }
        
        let eventDate = currentLocation.timestamp
        let howRecent = eventDate.timeIntervalSinceNow
        
        guard abs(howRecent) < 15 else {
            return
        }
        
        print("location: \(currentLocation.coordinate.latitude), \(currentLocation.coordinate.longitude)")
        
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(currentLocation, completionHandler: {
            (placemarks, error) -> Void in
            
            if error != nil {
                OpenWeatherHelper.sharedInstance.getCurrentWeatherForManila(locFoundHander)
                
                print("reverse geocoding error")
                
                return
            }
            
            var countryCode: String? = nil
            
            if placemarks?.count > 0 {
                countryCode = placemarks?.first?.isoCountryCode
                print("country code is: \(countryCode)")
            }
            
            if let code = countryCode, code == "PHL" {
                print("in PH")
                OpenWeatherHelper.sharedInstance.getCurrentWeatherForLocation(location: currentLocation, completion: locFoundHander)
            } else {
                print("not in PH")
                OpenWeatherHelper.sharedInstance.getCurrentWeatherForManila(locFoundHander)
            }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(#function)
        
        trackLocationIfProperAuthorization(currentAuthorization: status)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(#function)
        
        print("error encountered: \(error)")
    }
    
}
