//
//  PAGASATweetsTableViewController.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 16/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import UIKit
import TwitterKit

class PAGASATweetsTableViewController: TWTRTimelineViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let twitterClient = TWTRAPIClient()
        let pagasaDataSource = TWTRUserTimelineDataSource(screenName: "dost_pagasa", apiClient: twitterClient)
        self.dataSource = pagasaDataSource
        
        // Get the height of the status bar
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        
        let insets = UIEdgeInsets(top: statusBarHeight, left: 0, bottom: 0, right: 0)
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
        
        RunLoop.main.add(Timer.init(timeInterval: 120, target: self, selector: #selector(PAGASATweetsTableViewController.refreshTweetsAndTheme), userInfo: nil, repeats: true ), forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshTweetsAndTheme()
    }
    
    func refreshTweetsAndTheme() {
        updateTheme()
        
        self.refresh()
    }
    
    fileprivate func updateTheme() {
        TWTRTweetView.appearance().backgroundColor = AppTheme.bgColor
        TWTRTweetView.appearance().primaryTextColor = AppTheme.textColor
        TWTRTweetView.appearance().linkTextColor = AppTheme.linkTextColor
    }
}
