//
//  AboutViewController.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 20/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController, UITextViewDelegate {
    @IBOutlet var appTitle: UILabel!
    @IBOutlet var versionNumber: UILabel!
    @IBOutlet var weatherAPIAttribution: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weatherAPIAttribution.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateTheme()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        UIApplication.shared.openURL(URL)
        return false
    }
    
    func updateTheme() {
        view.backgroundColor = AppTheme.bgColor
        appTitle.textColor = AppTheme.textColor
        versionNumber.textColor = AppTheme.textColor
        
        weatherAPIAttribution.backgroundColor = AppTheme.bgColor
        
        let attributedString = NSMutableAttributedString(string: "Powered by OpenWeatherMap")
        attributedString.addAttribute(NSFontAttributeName,
                                      value: UIFont.systemFont(ofSize: 15),
                                      range: NSRange(location: 0, length: 25))
        attributedString.addAttribute(NSLinkAttributeName,
                                      value: "https://www.openweathermap.org",
                                      range: NSRange(location: 11, length: 14))
        
        let linkAttributes: [String: Any] = [
            NSForegroundColorAttributeName: AppTheme.linkTextColor,
            NSUnderlineColorAttributeName: AppTheme.underlineColor,
            NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue
        ]
        
        weatherAPIAttribution.linkTextAttributes = linkAttributes
        weatherAPIAttribution.attributedText = attributedString
        
        weatherAPIAttribution.textColor = AppTheme.textColor
    }
}
