//
//  PHWeatherViewController.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 14/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import UIKit

class PHWeatherViewController: UIViewController {
    
    @IBOutlet var cityNameLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var iconLabel: UILabel!
    
    let locationService = LocationService()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        RunLoop.main.add(Timer.init(timeInterval: 1800, target: self, selector: #selector(PHWeatherViewController.startLocServiceAndChangeBG), userInfo: nil, repeats: true ), forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startLocServiceAndChangeBG()
    }
    
    func startLocServiceAndChangeBG() {
        print("called by timer")
        locationService.startLocationServiceWithCompletionHandler() {
            (weatherResult) -> Void in
            
            switch weatherResult {
            case let .success(weather):
                OperationQueue.main.addOperation() {
                    let tempInCelsius = WeatherHelper.fahrenheitToCelsius(weather.temperature)
                    self.temperatureLabel.text = String(format: "%.2f", tempInCelsius) + " ℃"
                    
                    self.cityNameLabel.text = weather.city.capitalized
                    self.descriptionLabel.text = weather.description.capitalized
                    self.iconLabel.text = weather.icon
                    
                    self.changeBGColorForTempAndHumidity(weather.temperature, humidity: weather.humidity)
                }
            case let .failure(error):
                print("viewcontroller error: \(error)")
            }
        }
    }
    
    fileprivate func changeBGColorForTempAndHumidity(_ temp: Float, humidity: Float) {
        let color = TemperatureBasedColorGenerator.colorForTemperatureAndHumidity(fahrenheit: temp, humidity: humidity)
        view.backgroundColor = color
        
        AppTheme.bgColor = color
        AppTheme.textColor = UIColor.white
        AppTheme.linkTextColor = UIColor.black
    }
}
