//
//  FormatterHelper.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 19/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation

struct FormatterHelper {
    static let phDateFormatter: DateFormatter = {
        let df = DateFormatter()
        
        df.locale = Locale(identifier: "en_PH")
        
        return df
    }()
}
