//
//  AppTheme.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 20/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import UIKit

struct AppTheme {
    static var bgColor = UIColor.white
    static var textColor = UIColor.black
    static var linkTextColor = UIColor.blue
    static var underlineColor = UIColor.lightGray
}
