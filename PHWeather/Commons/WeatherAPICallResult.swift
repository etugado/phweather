//
//  WeatherAPICallResult.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 15/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation

enum WeatherJSONResult {
    case success(Weather)
    case failure(Error)
}

enum WeatherJSONError: Error {
    case rawJSONParseError
    case weatherAPINetworkError
}

typealias WeatherCompletionHandler = (WeatherJSONResult) -> Void
