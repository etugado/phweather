//
//  TemperatureBasedColorGenerator.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 19/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import UIKit

struct TemperatureBasedColorGenerator {
    fileprivate static var hue: CGFloat = 1.0
    fileprivate static var saturation: CGFloat = 0.85
    fileprivate static var brightness: CGFloat = 1.0
    fileprivate static var alpha: CGFloat = 1.0
    
    static func colorForTemperatureAndHumidity(fahrenheit: Float, humidity: Float) -> UIColor {
        let partOfDay = WeatherHelper.currentPartOfDay()
        let actualTemp = WeatherHelper.heatIndexForTemp(fahrenheit: fahrenheit, humidityInPercent: humidity)
        
        if partOfDay == PartOfDay.am {
            hue = dayHueForTemperature(fahrenheit: Float(actualTemp))
            
            print("day hue is: \(hue) for acualTemp: \(actualTemp) and temp: \(fahrenheit) and temp celsius: \(WeatherHelper.fahrenheitToCelsius(actualTemp))")
            
            return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
        } else {
            hue = nightHueForTemperature(fahrenheit: Float(actualTemp))
            
            print("night hue is: \(hue) for acualTemp: \(actualTemp) and temp: \(fahrenheit) and temp celsius: \(WeatherHelper.fahrenheitToCelsius(actualTemp))")
            
            return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
        }
    }
    
    fileprivate static func dayHueForTemperature(fahrenheit: Float) -> CGFloat {
        let celsius = WeatherHelper.fahrenheitToCelsius(fahrenheit)
        
        var hue: CGFloat = 0
        
        if celsius > 40 {
            hue = 0.025
        } else if celsius <= 40 && celsius > 30 {
            hue = CGFloat( -0.007 * celsius + 0.33 )
        } else if celsius <= 30 && celsius > 25 {
            hue = CGFloat( -0.056 * celsius + 1.8 )
        } else if celsius <= 25 && celsius > 20 {
            hue = CGFloat(-0.02 * celsius + 0.9 )
        } else if celsius <= 20 && celsius > 0 {
            hue = CGFloat(-0.005 * celsius + 0.6 )
        } else {
            hue = 0.6
        }
        
        return hue
    }
    
    fileprivate static func nightHueForTemperature(fahrenheit: Float) -> CGFloat {
        let celsius = WeatherHelper.fahrenheitToCelsius(fahrenheit)

        var hue: CGFloat = 0
        
        if celsius > 40 {
            hue = 0.975
        } else if celsius <= 40 && celsius > 30 {
            hue = CGFloat( 0.005 * celsius + 0.6 )
        } else if celsius <= 30 && celsius > 25 {
            hue = CGFloat( 0.01 * celsius + 0.45 )
        } else if celsius <= 25 && celsius > 20 {
            hue = CGFloat( 0.02 * celsius + 0.2 )
        } else if celsius <= 20 && celsius > 0 {
            hue = CGFloat( 0.0025 * celsius + 0.55 )
        } else {
            hue = 0.55
        }
        
        return hue
    }
    
}

