//
//  WeatherHelper.swift
//  PHWeather
//
//  Created by Emmanuel Francisco Tugado on 18/08/2016.
//  Copyright © 2016 Meow. All rights reserved.
//

import Foundation

struct WeatherHelper {
    static func kelvinToCelsius(_ kelvin: Float) -> Float {
        return kelvin  - 273.15
    }
    
    static func kelvinToFahrenheit(_ fahrenheit: Float) -> Float {
        return kelvinToCelsius(fahrenheitToCelsius(fahrenheit))
    }
    
    static func celsiusToFahrenheit(_ celsius: Float) -> Float {
        return (celsius * 9 / 5) + 32
    }
    
    static func fahrenheitToCelsius(_ fahrenheit: Float) -> Float {
        return (fahrenheit - 32) * 5 / 9
    }
    
    static func heatIndexForTemp(fahrenheit T: Float, humidityInPercent RH: Float) -> Float {
        let heatIndex = -42.379 + (2.04901523*T) + (10.14333127*RH) - (0.22475541*T*RH) - (0.00683783*T*T) - (0.05481717*RH*RH) + (0.00122874*T*T*RH) + (0.00085282*T*RH*RH) - (0.00000199*T*T*RH*RH)
        
        print("heatIndex is: \(heatIndex)")
        
        if RH > 85 && (T >= 80 && T <= 87) {
            let adjustment = ((RH-85)/10) * ((87-T)/5)
            print("getting heat adjustment: \(adjustment)")
            return heatIndex + adjustment
        }
        
        return heatIndex
    }
    
    static func currentPartOfDay() -> PartOfDay {
        let date = Date()
        let calendar = Calendar.current
        let currentHour = (calendar as NSCalendar).component(.hour, from: date)
        
        if currentHour >= PartOfDay.am.rawValue && currentHour < PartOfDay.pm.rawValue  {
            return .am
        } else {
            return .pm
        }
    }
}

enum PartOfDay: Int {
    case am = 6     // 6 am - sunrise
    case pm = 18    // 6 pm - sunset
}
